import sys
import json
import requests
import os
repo_name=sys.argv[1]
branch_name=sys.argv[2]
auth_token = sys.argv[3]
env_url = sys.argv[4]
description = sys.argv[5]
runnable_name = sys.argv[6]
not_include_flist=['controller.py','.git','.gitignore','bitbucket-pipelines.yml']
files = [('files',open(x,'rb')) for x in os.listdir('.') if x not in not_include_flist] 
for i in range(len(sys.argv)):
    print("Variable available=",sys.argv[i])
if auth_token:
    header = {
        'Authorization': 'Token ' + auth_token,
        'Accept': 'application/json'
        }
    data = {
        'message': description,
        'memory': '512mb',
        }
    #url = env_url + '/api/runnable/' + repo_name + '/' # for creating new runnable
    url = env_url + '/api/runnable/' + runnable_name + '/version/' # for creating new runnable
    response = requests.post(url, headers=header,data=data, files=files)
    print('******************',response)
    print('******************',response.json())
    print("Deployment successfully")
else:
    print("Deployment not successfull")